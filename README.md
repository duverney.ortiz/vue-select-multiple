# vue-selected

## Install

```bash
npm i vue-select-multiple-options
```

## Usage

```js
import VueSelectMultiple from 'vue-select-multiple-options'
```

```javascript
<template>
  <div id="app">
    <vue-select-multiple v-model="selections1" :Items="data" label="Área:" />
    <div class="flex">
      <vue-select-multiple v-model="selections1" :Items="data" Clases="input-customen" label="Área:" />
      <vue-select-multiple v-model="selections2" :Items="data" Clases="input-customen" label="Nombre:" />
      <vue-select-multiple
        v-model="selections3"
        :Items="data"
        :reduce="(_items) => _items.value"
        @dataReduce="dataReduce"
      />
    </div>
  </div>
</template>

<script>
  import Vue from "vue";
  import VueSelectMultiple from 'vue-select-multiple-options'

  export default Vue.extend({
    name: "ServeDev",

    components: {
      VueSelectMultiple,
    },
    data: () => ({
      selections1: [],
      selections2: [],
      selections3: [],
      data: [
        {
          value: 1,
          text: "MAMOGRAFÍA",
        },
        {
          value: 2,
          text: "ECOGRAFÍA",
        },
        {
          value: 3,
          text: "ODONTOLOGÍA",
        },
      ],
    }),
    methods: {
      dataReduce(data) {
        console.log("Este es el resultado de la data reduce: ", data);
      },
    },
  });
</script>



<style>
  .input-customen {
    height: 30px !important;
    border-color: red !important;
  }

  .flex {
    display: flex;
  }
</style>
```